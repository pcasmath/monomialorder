// Tests for errors.go

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package monomialorder

import (
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestError tests the behaviour of errors.
func TestError(t *testing.T) {
	e := objectError(0)
	if !e.Is(e) {
		t.Fatalf("expected Is to return true but got false")
	} else if e.Is(objectError(-1)) {
		t.Fatal("expected Is to return false but got true")
	}
}
