module bitbucket.org/pcasmath/monomialorder

go 1.16

require (
	bitbucket.org/pcasmath/abelianmonoid v0.0.1
	bitbucket.org/pcasmath/compare v0.0.4
	bitbucket.org/pcasmath/integer v0.0.1
	bitbucket.org/pcasmath/object v0.0.4
	bitbucket.org/pcasmath/rational v0.0.1
)
